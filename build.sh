#!/bin/sh -eu
for d in *.d ; do
  (
    cd "${d}"
    tgz=../${d%.d}.tgz
    unshare -rnm metastore --apply
    metastore --dump |
        awk '$7 ~ /[^/]$/ { print $7 }' |
        xargs unshare -rnm tar -cz > "${tgz}"
  )
done
